﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
        {
            int averageWordLength = 0;
            int wordsSummaryLength = 0;
            
            var tempString = new StringBuilder();
            

            if (string.IsNullOrEmpty(inputString))
            {
                return 0;
            }

            for (int i = 0; i < inputString.Length; i++)
            {
                if (inputString[i] == '\\')
                {
                    i++;
                    continue;
                }

                if (Char.IsLetter(inputString[i]) || inputString[i] == ' ')
                {
                    tempString.Append(inputString[i]);
                }
            }

            if (string.IsNullOrEmpty(tempString.ToString().Trim()))
            {
                return 0;
            }

            string[] words = tempString.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            foreach (string word in words)
            {
                wordsSummaryLength += word.Length;
            }

            averageWordLength = wordsSummaryLength / words.Length;

            return averageWordLength;
        }

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
            
            var temp = new StringBuilder(); 

            if (string.IsNullOrEmpty(toDuplicate))
            {
                return original;

            }

            if (original == null)
            {
                return null;
            }

            foreach (char ch in original)
            {
                if (!toDuplicate.Contains(ch, StringComparison.CurrentCultureIgnoreCase))
                {
                    temp.Append(ch);
                }
                else
                {
                    temp.Append(ch);
                    temp.Append(ch);
                }
            }

            original = temp.ToString();
            return original;
        }
	}
}