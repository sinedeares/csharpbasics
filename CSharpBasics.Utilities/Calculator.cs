﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
        /// <summary>
        /// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
        /// </summary>
        /// <param name="number">Натуральное число</param>
        /// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
        /// <returns>Вычисленная сумма</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
        /// а также если массив <see cref="divisors"/> пустой
        /// </exception>
        public float CalcSumOfDivisors(int number, params int[] divisors)
        {
            int sum = 0;
            if (number <= 0 || divisors.GetLength(0) == 0)
            {
                throw new ArgumentOutOfRangeException($"Массив divisors пустой или переданное число не является натуральным");
            }

            for (int i = 0; i < divisors.GetLength(0); i++)
            {

                if (divisors[i] > 0)
                {
                    for (int digit = 1; digit < number; digit++)
                    {
                        if (digit % divisors[i] == 0)
                        {
                            sum += digit;
                        }
                    }

                }
                else throw new ArgumentOutOfRangeException($"Числа массива divisors не являются натуральынми");
            }
            return sum;
        }

        /// <summary>
        /// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
        /// </summary>
        /// <param name="number">Исходное число</param>
        /// <returns>Ближайшее наибольшее целое</returns>
        public long FindNextBiggerNumber(int number)
		{
            if (number < 1)
            {
                throw new ArgumentOutOfRangeException($"Переданное число меньше 1");
            }

            if ( number < 10 || number % 10 == 0)
            {
                throw new InvalidOperationException($"Число не должно состоять из одной цифры, а так же не должно оканчиваться нулём");
            }

            List<int> resultList = new List<int>();
            var stringResult = new StringBuilder();
            
            long result = 0;

            while (number != 0)
            {
                resultList.Add(number % 10);
                number /= 10;
            }

            var resultArray = resultList.ToArray();
            Array.Reverse(resultArray);

            int index= 0;
            for (int i = resultArray.Length - 1; i > 0; i--)
            {
                if (resultArray[i] > resultArray[i-1])
                {
                    (resultArray[i], resultArray[i - 1]) = 
                        (resultArray[i - 1], resultArray[i]);
                    index = i;
                    break;
                }
            }

            Array.Sort(resultArray, index, resultArray.Length - index);

            for (long i = 0; i <= resultArray.LongLength - 1; i++)
            {
                stringResult.Append(resultArray[i].ToString());
            }

            result = long.Parse(stringResult.ToString());
            

            return result;
        }
	}
}